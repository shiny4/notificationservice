﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NotificationService.Entities.Models;
using NotificationService.Infrastructure.Interfaces.Services;

namespace NotificationService.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private readonly IEmailService _emailService;

        public EmailController(IEmailService emailService)
        {
            _emailService = emailService;
        }

        [Authorize]
        [HttpPost]
        public IActionResult Post(Email email)
        {
            _emailService.SendEmailAsync(email);
            return Ok();
        }
    }
}
