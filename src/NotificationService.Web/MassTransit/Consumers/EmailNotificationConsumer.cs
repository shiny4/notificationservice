﻿using System.Threading.Tasks;
using MassTransit;
using Jumping.Contracts;
using MassTransit.Definition;
using Microsoft.Extensions.Logging;
using NotificationService.Infrastructure.Interfaces.MassTransit.Constans;
using NotificationService.Infrastructure.Interfaces.Services;

namespace MassTransit.Consumers
{
    public class EmailNotificationConsumer: IConsumer<EmailNotification>
    {
        private readonly IEmailService _emailService;
        private readonly ILogger<EmailNotificationConsumer> _logger;

        public EmailNotificationConsumer(IEmailService emailService, ILogger<EmailNotificationConsumer> logger)
        {
            _emailService = emailService;
            _logger = logger;
        }

        public Task Consume(ConsumeContext<EmailNotification> context)
        {
            EmailNotification email = context.Message;
            if (string.IsNullOrWhiteSpace(email.Address) || string.IsNullOrWhiteSpace(email.Body) ||
                string.IsNullOrWhiteSpace(email.Subject))
                return Task.CompletedTask;
            
            _emailService.SendEmailNotificationAsync(email);
            return Task.CompletedTask;
        }
    }

    public class EmailNotificationConsumerDefinition : ConsumerDefinition<EmailNotificationConsumer>
    {
        public EmailNotificationConsumerDefinition()
        {
            EndpointName = MassTransitConstans.EmailNotificationServiceQueue;
        }
    }
}
