namespace NotificationService.Web.Options
{
    public class JWTOptions
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public string Secret { get; set; }
    }
}