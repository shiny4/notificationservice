﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using NotificationService.Entities.Models;
using Jumping.Contracts;
using Microsoft.Extensions.Logging;
using NotificationService.Infrastructure.Interfaces.Options;
using NotificationService.Infrastructure.Interfaces.Services;

namespace NotificationService.Web.Services
{
    public class EmailService : IEmailService
    {
        private readonly EmailOptions _emailOptions;
        private static ILogger<EmailService> _logger;
        private  readonly uint _key = 0xa1839fd2; 

        public EmailService(IOptions<EmailOptions> emailOptions, ILogger<EmailService> logger)
        {
            _logger = logger;
            _emailOptions = emailOptions.Value;
        }
        public void SendEmailAsync(Email email)
        {
            SendMailAsync(email);
        }

        public void SendEmailNotificationAsync(EmailNotification email)
        {
            Email newEmail = new Email()
            {
                Address = email.Address,
                Body = email.Body,
                Subject = email.Subject,
                IsSended = false,
                Attempts = 0
            };
            SendMailAsync(newEmail);
        }
        
        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
             Email email = (Email) e.UserState;

            if (e.Cancelled)
            {
                 Console.WriteLine("[{0}] Send canceled.", email);
                 _logger.LogInformation("Mail for adress='{EmailTo}' with subject='{MailSubject}' was cancelled",
                     email.Address, email.Subject);
            }
            if (e.Error != null)
            {
                _logger.LogError("Mail for adress='{EmailTo}' with subject='{MailSubject}' was not sent. Error: {MailError} ",
                    email.Address, email.Subject,e.Error.ToString());
            } else
            {
                _logger.LogInformation("Mail for adress='{EmailTo}' with subject='{MailSubject}' was sent succesfully",
                    email.Address, email.Subject);
                email.IsSended = true;
            }
        }
        private void SendMailAsync(Email email)
        {
            // Command-line argument must be the SMTP host.
            SmtpClient client = new SmtpClient(_emailOptions.Server, _emailOptions.Port);
            var emailfrom = EncodeDecrypt(_emailOptions.Email, _key);
            var emailpwd = EncodeDecrypt(_emailOptions.Password, _key);
            client.Credentials = new NetworkCredential(emailfrom, emailpwd);
            client.EnableSsl = true;

            // Specify the email sender.
            // Create a mailing address that includes a UTF8 character in the display name.
            
            MailAddress from = new MailAddress(emailfrom, _emailOptions.FromName,
            System.Text.Encoding.UTF8);
            // Set destinations for the email message.
            MailAddress to = new MailAddress(email.Address);
            // Specify the message content.
            MailMessage message = new MailMessage(from, to);
            message.Body = email.Body;
            message.BodyEncoding =  System.Text.Encoding.UTF8;
            message.Subject = email.Subject ;
            message.SubjectEncoding = System.Text.Encoding.UTF8;
            email.Attempts += 1;
            
            // Set the method that is called back when the send operation ends.
            client.SendCompleted += new
            SendCompletedEventHandler(SendCompletedCallback);
            // The userState can be any object that allows your callback
            // method to identify this send operation.
            // For this example, the userToken is a string constant.
            _logger.LogInformation("Try to sent mail for adress='{EmailTo}' with subject='{MailSubject}'",
                to, message.Subject);
            client.SendAsync(message,email);
        }
        
        public static string EncodeDecrypt(string str, uint secretKey)
        {
            var ch = str.ToArray(); //преобразуем строку в символы
            string newStr = "";      //переменная которая будет содержать зашифрованную строку
            foreach (var c in ch)  //выбираем каждый элемент из массива символов нашей строки
                newStr += TopSecret(c, secretKey);  //производим шифрование каждого отдельного элемента и сохраняем его в строку
            return newStr;
        }

        public static char TopSecret(char character, uint secretKey)
        {
            character = (char)(character ^ secretKey); //Производим XOR операцию
            return character;
        }

    }
}