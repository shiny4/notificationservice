﻿using System;

namespace NotificationService.Entities.Models
{
    public class Email
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsSended { get; set; } = false;
        public int Attempts { get; set; } = 0;
    }


}
