﻿using System;
using System.Threading.Tasks;
using NotificationService.Entities.Models;
using Jumping.Contracts;

namespace NotificationService.Infrastructure.Interfaces.Services
{
    public interface IEmailService
    {
        void SendEmailAsync(Email email);
        void SendEmailNotificationAsync(EmailNotification email);
    }
}
