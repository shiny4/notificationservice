namespace Jumping.Contracts
{
    public interface IEmailNotification
    {
        string Address { get; set; }
        string Subject { get; set; }
        string Body { get; set; }
    }
}